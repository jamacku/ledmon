#!/bin/bash

source tc.sh || exit 200

### return /dev/sda
function get_disk(){
       local disk_lists=""
       local first_disk=""
       tlog "get disk from local"
       for i in $(lsblk |grep disk |awk '{print $1}');do
           disk_lists="$disk_lists $i"
       done
       tlog "disk list=$disk_lists"
       RETURN_STR="$disk_lists"
}

uname -a

rpm -q ledmon
if [[ $? != 0 ]]; then
    echo "ledmon is not installed" >&2
    exit 1
fi

>/var/log/ledmon.log
>/var/log/ledctl.log

get_disk
disks=$RETURN_STR
tlog "disks=$disks"
tlog "get the first disk"
first_disk=$(echo $disks |awk '{print $1}')
if [ $first_disk == "" ];then
    echo "can not get disk from get_disk function"
    exit 0
fi

first_disk="/dev/$first_disk"

tlog "we will force in $first_disk"

tok "ledctl locate=$first_disk"
tok "ledctl locate_off=$first_disk"
tok "ledctl locate=$first_disk"
tok "ledctl off={ $first_disk }"

tok "ledctl degraded=$first_disk"
tok "ledctl rebuild=$first_disk"
tok "ledctl hotspare=$first_disk"
tok "ledctl pfa=$first_disk"
tok "ledctl failure=$first_disk"
tok "ledctl off={ $first_disk }"

tlog "ledctl on ses "
for i in ses_rebuild ses_ifa ses_ica ses_cons_check ses_hotspare ses_rsvd_dev \
    ses_ok ses_ident ses_rm ses_insert ses_missing ses_dnr ses_devoff ses_fault ses_active ;do
    tok "ledctl $i=$first_disk --log=/var/log/ledctl.log"
    # don't sleep after skipped tests
    if [[ $? != 2 ]]; then
        sleep 2
    fi
done
tok "ledctl off=$first_disk"

tlog "ledmon testing"
trun "ledmon --log=/var/log/ledmon.log"
sleep 2

trun "cat /var/log/ledmon.log"
trun "cat /var/log/ledctl.log"

tend
